import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import AppNavbar from "./components/AppNavBar";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Product from "./pages/Products";
import { UserProvider } from "./userContext";
import { useState, useEffect } from "react";
function App() {
  // State Hook for the user state thats deficed here for a global scope
  const [user, setUser] = useState({
    // email: localStorage.getItem("email"),
    id: null,
    isAdmin: null,
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Product />} />
            <Route path="/login" element={<Login />} />
            <Route path="*" element={<h1>No routes found</h1>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
